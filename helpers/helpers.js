var digits = new Array(
	'0',
	'0',
	'0',
	'0',
	'0',
	'0',
	'0',
	'0',
	'0',
	'0',
	'0',
	'0',
	'0',
	'0',
	'0',
	'0'
);
var words = new Array(
	'',
	'Satu',
	'Dua',
	'Tiga',
	'Empat',
	'Lima',
	'Enam',
	'Tujuh',
	'Delapan',
	'Sembilan'
);
var levels = new Array('', 'Ribu', 'Juta', 'Milyar', 'Triliun');

module.exports = { digits, words, levels };
