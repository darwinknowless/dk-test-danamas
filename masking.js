/*\b matches word boundaries.
[^\W] matches any non-whitespace characters
{2} tells it to find 2 occurrences, {1,2} means 1 or 2, {2,} means 2 or more
g at the end is for global (matches all applicable words in the string)
$1 in the replacement matches a group captured with parentheses */
function masking(kata) {
	var regex1 = /\b[^\W]{2}([^\W]{1,2})\b/g;
	var regex2 = /\b[^\W]{2}([^\W]{2,})[^\W]\b/g;
	return kata.replace(regex1, '**$1').replace(regex2, '**$1*');
}

// console.log(masking('Coding'));
// console.log(masking('Danamas'));
// console.log(masking('Coding'));
// console.log(masking('Cristiano Ronaldo'));
// console.log(masking('PT Makmur Maju Mundur'));

module.exports = masking;
