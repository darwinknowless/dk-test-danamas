const terbilang = require('../angkaToBilangan');

test('Input => 1, Output => Satu', () => {
	expect(terbilang('1'));
});

test('Input => 2, Output => Dua', () => {
	expect(terbilang('2'));
});

test('Input => 3, Output => Tiga', () => {
	expect(terbilang('3'));
});

test('Input => 102, Output => Seratus Dua', () => {
	expect(terbilang('102'));
});

test('Input => 5088, Output => Lima Ribu Delapan Puluh Delapan', () => {
	expect(terbilang('5088'));
});

test('Input => 123456789, Output => Seratus Dua Puluh Tiga Juta Empat Ratus Lima Puluh Enam Ribu Tujuh Ratus Delapan Puluh Sembilan', () => {
	expect(terbilang('123456789'));
});
