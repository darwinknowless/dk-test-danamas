const masking = require('../masking');

test('Input => Coding, Output => **din*', () => {
	expect(masking('Coding'));
});

test('Input => Danamas, Output => **nama*', () => {
	expect(masking('Danamas'));
});

test('Input => Cristiano Ronaldo, Output => **istian* **nald*', () => {
	expect(masking('Cristiano Ronaldo'));
});

test('Input => PT Maju Mundur Makmur, Output => PT **kmu* **ju **ndu*', () => {
	expect(masking('PT Maju Mundur Makmur'));
});
