const { digits, words, levels } = require('./helpers/helpers');

function terbilang(bilangan, sufix) {
	if (
		bilangan == '' ||
		bilangan == null ||
		bilangan == 'null' ||
		bilangan == undefined
	) {
		return '';
	} else {
		bilangan = bilangan.replace(/[^,\d]/g, '');
		var kalimat = '';
		var angka = digits;
		var kata = words;
		var tingkat = levels;
		var panjang_bilangan = bilangan.length;

		/* pengujian panjang bilangan */
		if (panjang_bilangan > 15) {
			kalimat = 'Diluar Batas';
		} else {
			/* mengambil angka-angka yang ada dalam bilangan, dimasukkan ke dalam array */
			for (i = 1; i <= panjang_bilangan; i++) {
				angka[i] = bilangan.substr(-i, 1);
			}

			var i = 1;
			var j = 0;

			/* mulai proses iterasi terhadap array angka */
			while (i <= panjang_bilangan) {
				subkalimat = '';
				kata1 = '';
				kata2 = '';
				kata3 = '';

				/* untuk Ratusan */
				if (angka[i + 2] != '0') {
					if (angka[i + 2] == '1') {
						kata1 = 'Seratus';
					} else {
						kata1 = kata[angka[i + 2]] + ' Ratus';
					}
				}

				/* untuk Puluhan atau Belasan */
				if (angka[i + 1] != '0') {
					if (angka[i + 1] == '1') {
						if (angka[i] == '0') {
							kata2 = 'Sepuluh';
						} else if (angka[i] == '1') {
							kata2 = 'Sebelas';
						} else {
							kata2 = kata[angka[i]] + ' Belas';
						}
					} else {
						kata2 = kata[angka[i + 1]] + ' Puluh';
					}
				}

				/* untuk Satuan */
				if (angka[i] != '0') {
					if (angka[i + 1] != '1') {
						kata3 = kata[angka[i]];
					}
				}

				/* pengujian angka apakah tidak nol semua, lalu ditambahkan tingkat */
				//TODO : kata1(Ratusan) + kata2(Puluhan) + kata3(Satuan) + tingkat[ribu, juta, milyar, triliun]
				if (angka[i] != '0' || angka[i + 1] != '0' || angka[i + 2] != '0') {
					subkalimat =
						kata1 + ' ' + kata2 + ' ' + kata3 + ' ' + tingkat[j] + ' ';
				}

				/* gabungkan variabe sub kalimat (untuk Satu blok 3 angka) ke variabel kalimat */
				kalimat = subkalimat + kalimat;
				i = i + 3;
				j = j + 1;
			}

			/* mengganti Satu Ribu jadi Seribu jika diperlukan */
			if (angka[5] == '0' && angka[6] == '0') {
				kalimat = kalimat.replace('Satu Ribu', 'Seribu');
			}
		}

		// if u wanna add Rupiah
		return sufix == undefined ? kalimat : kalimat + sufix;
	}
}

module.exports = terbilang;
