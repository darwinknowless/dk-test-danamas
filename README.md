# Danamas Assesment

Folder/File/Repo ini berisi tugas/assesment, untuk melengkapi interview sebagai Software Engineer di Danamas.

## Authors

- [@Darwin](https://www.gitlab.com/darwinknowless)

## Running Tests

To run tests, run the following command

```bash
  npm run test
```
